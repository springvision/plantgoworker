//
//  TimeOffApplyViewController.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/6/28.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeOffApplyViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIPickerView *timePicker;
@property (weak, nonatomic) IBOutlet UIButton *endTimeBtn;
@property (weak, nonatomic) IBOutlet UIButton *startTimeBtn;

@end
