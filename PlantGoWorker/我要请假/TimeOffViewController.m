//
//  TimeOffViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/6/28.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "TimeOffViewController.h"
#import "TimeOffTableViewCell.h"
#import "TimeOffApplyViewController.h"

@interface TimeOffViewController ()
{
    NSArray *_dataArr;
    int _limit;
}
@end

@implementation TimeOffViewController

- (void)viewDidLoad {
    _limit = 20;
    [super viewDidLoad];
    [self loadData];
    [self initTabView];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)loadData{
    NSString *token = [GlobalVar getVarName:@"token"];
    NSDictionary *postDic = @{
                              @"token":token,
                              @"begin":@0,
                              @"limit":[NSNumber numberWithInt:_limit]
                              };
    [SendRequestClass commonRequest:@"/api/myofftimelist" postDic:postDic completionBlock:^(NSDictionary *succDic) {
        if ([[succDic objectForKey:@"code"] intValue] == SUCC_RESP)
        {
            NSLog(@"Success MyOffTimeList");
            _dataArr = [succDic objectForKey:@"data"];
            [_tabView reloadData];
        }
        else{
            NSLog(@"Fail MyOffTimeList");
            [MyHint showHint:[succDic objectForKey:@"message"]];
        }
        
    } failureBlock:^(NSDictionary *failDic) {
        NSLog(@"Network error");
        [MyHint showHint:@"网络出现故障"];
    }];
}


#pragma mark - UITabView
- (void)initTabView
{
    _tabView.rowHeight = 70;
//    _tabView.bounces = NO;
    [_tabView registerNib:[UINib nibWithNibName:@"TimeOffTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    _tabView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self loadData];
        [_tabView.mj_header endRefreshing];
    }];
    
    _tabView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        _limit += 20;
        [self loadData];
        [_tabView.mj_footer endRefreshing];
    }];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataArr count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellData = [_dataArr objectAtIndex:indexPath.row];
    
    TimeOffTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell.dateLabel setText:[cellData objectForKey:@"leaveDate"]];
    [cell.fromLabel setText:[cellData objectForKey:@"startTime"]];
    [cell.toLabel   setText:[cellData objectForKey:@"endTime"]];
    [cell.fromLabel adjustsFontSizeToFitWidth];
    [cell.toLabel adjustsFontSizeToFitWidth];
    switch ([[cellData objectForKey:@"state"] intValue]) {
        case 1:
            [cell.stateImgView setImage:[UIImage imageNamed:@"offtime_unparsed.png"]];
            break;
        case 2:
            [cell.stateImgView setImage:[UIImage imageNamed:@"offtime_agree.png"]];
            break;
        case 3:
            [cell.stateImgView setImage:[UIImage imageNamed:@"offtime_deny.png"]];
            break;
        default:
            break;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:{

            break;
        }
        default:
            break;
    }
}

- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)applyBtnClick:(id)sender {
    TimeOffApplyViewController *vc = [[TimeOffApplyViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    
}
@end
