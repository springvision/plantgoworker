//
//  TimeOffApplyViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/6/28.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "TimeOffApplyViewController.h"
#import "ZFChooseTimeCollectionViewCell.h"
#import "ZFTimerCollectionReusableView.h"


static NSString * const reuseIdentifier = @"ChooseTimeCell";
static NSString * const headerIdentifier = @"headerIdentifier";



@interface TimeOffApplyViewController ()
{
    int  _curTimeBtn;
}
@property (nonatomic, strong) NSDate *date;

@property (nonatomic, strong) NSDateFormatter *formatter;
@property (nonatomic, strong) NSDateComponents *comps;
@property (nonatomic, strong) NSCalendar *calender;
@property (nonatomic, strong) NSArray * weekdays;
@property (nonatomic, strong) NSTimeZone *timeZone;
@property (nonatomic, strong) NSArray *OutDateArray;
@property (nonatomic, strong) NSArray *selectedData;

@property (nonatomic, strong) NSString *selDateString;

@end

@implementation TimeOffApplyViewController

@synthesize date = newDate;

#define TIME_CHOICE @[@"09:00", @"09:15", @"09:30", @"09:45",\
                      @"10:00", @"10:15", @"10:30", @"10:45",\
@"11:00", @"11:15", @"11:30", @"11:45",\
@"12:00", @"12:15", @"12:30", @"12:45",\
@"13:00", @"13:15", @"13:30", @"13:45",\
@"14:00", @"14:15", @"14:30", @"14:45",\
@"15:00", @"15:15", @"15:30", @"15:45",\
@"16:00", @"16:15", @"16:30", @"16:45",\
@"17:00", @"17:15", @"17:30", @"17:45",\
@"18:00"\
]


#pragma mark ---
#pragma mark --- 初始化
- (NSTimeZone*)timeZone
{
    
    if (_timeZone == nil) {
        [UIColor blueColor];
        _timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
    }
    return _timeZone;
}


- (NSArray*)weekdays
{
    
    if (_weekdays == nil) {
        
        _weekdays = [NSArray arrayWithObjects: [NSNull null], @"0", @"1", @"2", @"3", @"4", @"5", @"6", nil];
        
    }
    return _weekdays;
}
- (NSCalendar*)calender
{
    
    if (_calender == nil) {
        
        _calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    }
    
    return _calender;
}
- (NSDateComponents*)comps
{
    
    if (_comps == nil) {
        
        _comps = [[NSDateComponents alloc] init];
        
    }
    
    return _comps;
}
- (NSDateFormatter*)formatter
{
    
    if (_formatter == nil) {
        
        _formatter =[[NSDateFormatter alloc]init];
        [_formatter setDateFormat:@"yyyy-MM-dd"];
    }
    return _formatter;
}


#pragma mark ---
#pragma mark --- 各种方法
/**
 *  根据当前月获取有多少天
 *
 *  @param dayDate 但前时间
 *
 *  @return 天数
 */
- (NSInteger)getNumberOfDays:(NSDate *)dayDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    
    NSRange range = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:dayDate];
    
    return range.length;
    
}
/**
 *  根据前几月获取时间
 *
 *  @param date  当前时间
 *  @param month 第几个月 正数为前  负数为后
 *
 *  @return 获得时间
 */
- (NSDate *)getPriousorLaterDateFromDate:(NSDate *)date withMonth:(int)month

{
    [self.comps setMonth:month];
    
    NSDate *mDate = [self.calender dateByAddingComponents:self.comps toDate:date options:0];
    return mDate;
    
}



/**
 *  根据时间获取周几
 *
 *  @param inputDate 输入参数是NSDate，
 *
 *  @return 输出结果是星期几的字符串。
 */
- (NSString*)weekdayStringFromDate:(NSDate*)inputDate {
    
    
    
    
    [self.calender setTimeZone: self.timeZone];
    
    NSCalendarUnit calendarUnit = NSCalendarUnitWeekday;
    
    NSDateComponents *theComponents = [self.calender components:calendarUnit fromDate:inputDate];
    
    return [self.weekdays objectAtIndex:theComponents.weekday];
    
}
/**
 *  获取第N个月的时间
 *
 *  @param currentDate 当前时间
 *  @param index 第几个月 正数为前  负数为后
 *
 *  @return @“2016年3月”
 */
- (NSArray*)timeString:(NSDate*)currentDate many:(NSInteger)index;
{
    
    NSDate *getDate =[self getPriousorLaterDateFromDate:currentDate withMonth:index];
    
    NSString  *str =  [self.formatter stringFromDate:getDate];
    
    return [str componentsSeparatedByString:@"-"];
}

/**
 *  根据时间获取第一天周几
 *
 *  @param dateStr 时间
 *
 *  @return 周几
 */
- (NSString*)getMonthBeginAndEndWith:(NSDate *)dateStr{
    
    double interval = 0;
    NSDate *beginDate = nil;
    NSDate *endDate = nil;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar setFirstWeekday:2];//设定周一为周首日
    BOOL ok = [calendar rangeOfUnit:NSCalendarUnitMonth startDate:&beginDate interval:&interval forDate:dateStr];
    //分别修改为 NSDayCalendarUnit NSWeekCalendarUnit NSYearCalendarUnit
    if (ok) {
        endDate = [beginDate dateByAddingTimeInterval:interval-1];
    }else {
        return @"";
    }
    
    return   [self weekdayStringFromDate:beginDate];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    newDate =[NSDate date];
    
    self.title = @"日期选择";
    
    float cellw =kDeviceWidth/7;
    
//    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
//    [flowLayout setItemSize:CGSizeMake(cellw, cellw*4/3)];//设置cell的尺寸
//    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];//设置其布局方向
//    [flowLayout setHeaderReferenceSize:CGSizeMake(kDeviceWidth, 50)];
//    [flowLayout setMinimumInteritemSpacing:0]; //设置 y 间距
//    [flowLayout setMinimumLineSpacing:0]; //设置 x 间距

    //UIEdgeInsetsMake(设置上下cell的上间距,设置cell左距离,设置上下cell的下间距,设置cell右距离);
    
    //其布局很有意思，当你的cell设置大小后，一行多少个cell，由cell的宽度决定
    

    _collectionView.backgroundColor = [UIColor whiteColor];
    UICollectionViewFlowLayout *flowLayout = [_collectionView collectionViewLayout];
    [flowLayout setMinimumLineSpacing:0];
    [flowLayout setMinimumInteritemSpacing:0];
    
    //    注册cell
    [_collectionView registerNib:[UINib nibWithNibName:@"ZFTimerCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerIdentifier];
    [_collectionView registerNib:[UINib nibWithNibName:@"ZFChooseTimeCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:reuseIdentifier];
  
    
}



#pragma mark ---
#pragma mark --- <UICollectionViewDataSource>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    CGSize size={kDeviceWidth,50};
    return size;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 5;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSDate *dateList = [self getPriousorLaterDateFromDate:newDate withMonth:section];
    
    NSString *timerNsstring = [self getMonthBeginAndEndWith:dateList];
    NSInteger p_0 = [timerNsstring integerValue];
    NSInteger p_1 = [self getNumberOfDays:dateList] + p_0;
    
    return p_1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ZFChooseTimeCollectionViewCell *cell =[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    NSDate*dateList = [self getPriousorLaterDateFromDate:newDate withMonth:indexPath.section];
    
    NSArray*array = [self timeString:newDate many:indexPath.section];
    
    NSInteger p = indexPath.row -[self getMonthBeginAndEndWith:dateList].intValue+1;
    
    NSString *str;
    
    if (p<10) {
        
        str = p > 0 ? [NSString stringWithFormat:@"0%d",p]:[NSString stringWithFormat:@"-0%d",-p];
        
    }else{
        
        str = [NSString stringWithFormat:@"%d",p];
    }
    
    
    NSArray *list = @[ array[0], array[1], str];
    
    [cell updateDay:list outDate:self.OutDateArray selected:[self.selectedData componentsJoinedByString:@""].integerValue currentDate:[self timeString:newDate many:0]];
    
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDate*dateList = [self getPriousorLaterDateFromDate:newDate withMonth:indexPath.section];
    NSInteger p = indexPath.row -[self getMonthBeginAndEndWith:dateList].intValue+1;
    NSArray *array = [self timeString:newDate many:indexPath.section];
    
    NSString *str = p < 10 ? [NSString stringWithFormat:@"0%d",p]:[NSString stringWithFormat:@"%d",p];
    
    self.OutDateArray =@[array[0],array[1],str];
    self.selectedData =self.OutDateArray;
    NSString *dateStr = [NSString stringWithFormat:@"%@年%@月%@日",array[0],array[1],str ];
    _selDateString = [NSString stringWithFormat:@"%@-%@-%@",array[0],array[1],str ];
    [_dateLabel setText:dateStr];
    [self.collectionView reloadData];
    
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        
        ZFTimerCollectionReusableView * headerCell = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerIdentifier forIndexPath:indexPath];
        
        [headerCell updateTimer:[self timeString:newDate many:indexPath.section]];
        
        return headerCell;
    }
    return nil;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float cellw =kDeviceWidth/7;
    return CGSizeMake(cellw, cellw);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    
    return UIEdgeInsetsMake(5, 0, 5, 0);
}


// 返回多少列
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// 返回每列的行数
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [TIME_CHOICE count];
}

// 返回每行的标题
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [TIME_CHOICE objectAtIndex:row];
}

// 选中行显示在label上
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (_curTimeBtn == 0){
        [_startTimeBtn setTitle:[TIME_CHOICE objectAtIndex:row] forState:UIControlStateNormal];
    }else{
        [_endTimeBtn setTitle:[TIME_CHOICE objectAtIndex:row] forState:UIControlStateNormal];
    }
    
    [_timePicker setHidden:YES];
}

- (IBAction)timeChoiceBtnClick:(id)sender {
    [_timePicker setHidden:NO];
    _curTimeBtn = ((UIButton*)sender).tag;
}


- (IBAction)commitBtnClick:(id)sender {
    NSString *startTime = _startTimeBtn.titleLabel.text;
    NSString *endTime = _endTimeBtn.titleLabel.text;
    if ([startTime compare:endTime] == NSOrderedAscending){
        // 可以提交请假
        NSDictionary *postDic = @{
                    @"token":[GlobalVar getVarName:@"token"],
                    @"leaveDate":_selDateString,
                    @"startTime":[startTime stringByAppendingString:@":00"],
                    @"endTime":[endTime stringByAppendingString:@":00"]
                    };
        [SendRequestClass commonRequest:@"/api/commitofftime" postDic:postDic completionBlock:^(NSDictionary *succDic) {
            if ([[succDic objectForKey:@"code"] intValue] == SUCC_RESP)
            {
                NSLog(@"Success MyOffTimeList");
                [MyHint showHint:@"您的请假已经提交"];
                [self.navigationController popViewControllerAnimated:YES];
            }
            else{
                NSLog(@"Fail MyOffTimeList");
                [MyHint showHint:[succDic objectForKey:@"message"]];
            }
            
        } failureBlock:^(NSDictionary *failDic) {
            NSLog(@"Network error");
            [MyHint showHint:@"网络出现故障"];
        }];

    }else{
        [MyHint showHint:@"时间不可以倒流"];
    }
}
- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
