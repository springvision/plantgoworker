//
//  NSObject+SendRequestClass.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/6/28.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"


@interface SendRequestClass : NSObject

//#define BASEURL @"http://218.2.197.230:12283"
#define BASEURL @"http://218.2.197.230:2083"

+ (void)commonRequest:(NSString*)path postDic:(NSDictionary*)postDic completionBlock:(void(^)(NSDictionary *dic))succblock failureBlock:(void (^)(NSDictionary *))failureBlock;

@end
