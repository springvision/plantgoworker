//
//  NSObject+SendRequestClass.m
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/6/28.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "SendRequestClass.h"


@implementation SendRequestClass : NSObject


+ (void)commonRequest:(NSString*)path postDic:(NSDictionary*)postDic completionBlock:(void(^)(NSDictionary *dic))succblock failureBlock:(void (^)(NSDictionary *))failureBlock
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString *url = [BASEURL stringByAppendingString:path];
    NSLog(@"Begin Request url:%@", url);
    UIActivityIndicatorView* activityIndicatorView = [ [ UIActivityIndicatorView alloc ]
                                                      initWithFrame:[[UIScreen mainScreen] bounds]];
    activityIndicatorView.activityIndicatorViewStyle= UIActivityIndicatorViewStyleWhiteLarge;
    activityIndicatorView.backgroundColor = [UIColor grayColor];
    activityIndicatorView.alpha = 0.4;
    [[[[UIApplication sharedApplication] keyWindow] rootViewController].view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    [manager POST:url parameters:postDic progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSDictionary*dic = responseObject;
        [activityIndicatorView stopAnimating];
        [activityIndicatorView removeFromSuperview];
        succblock(dic);
        
    } failure:^(NSURLSessionTask *task, NSError *error) {
        //网络不通提示
        [activityIndicatorView stopAnimating];
        [activityIndicatorView removeFromSuperview];
        failureBlock(@{@"code":@"1", @"failinfo":@"网络不通"});
    }];
    
    
}
@end
