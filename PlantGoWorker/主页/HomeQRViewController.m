//
//  HomeQRViewController.m
//  HuiJu
//
//  Created by 纪翀 on 16/2/24.
//  Copyright © 2016年 恒宇博基. All rights reserved.
//

#import "HomeQRViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "ResourceDetailViewController.h"

@interface HomeQRViewController ()<AVCaptureMetadataOutputObjectsDelegate>
{
    NSTimer *_timer;
}
//捕捉会话
@property (nonatomic, strong) AVCaptureSession *captureSession;
//展示layer
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
//
@property (strong, nonatomic) CALayer *scanLayer;


@end

@implementation HomeQRViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view from its nib.
    _scanLayer = nil;
    _captureSession = nil;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self startReading];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)startReading {
    NSError *error;
    
    //1.初始化捕捉设备（AVCaptureDevice），类型为AVMediaTypeVideo
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    //2.用captureDevice创建输入流
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    if (!input) {
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    //3.创建媒体数据输出流
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    
    //4.实例化捕捉会话
    _captureSession = [[AVCaptureSession alloc] init];
    
    //4.1.将输入流添加到会话
    [_captureSession addInput:input];
    
    //4.2.将媒体输出流添加到会话中
    [_captureSession addOutput:captureMetadataOutput];
    
    //5.创建串行队列，并加媒体输出流添加到队列当中
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    //5.1.设置代理
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    
    //5.2.设置输出媒体数据类型为QRCode
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObjects:AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode128Code, AVMetadataObjectTypeQRCode, nil]];
    
    //6.实例化预览图层
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    
    //7.设置预览图层填充方式
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    //8.设置图层的frame
    [_videoPreviewLayer setFrame:_cameraView.layer.bounds];
    
    //9.将图层添加到预览view的图层上
    [_cameraView.layer addSublayer:_videoPreviewLayer];
    
    
    //10.设置扫描范围
    CGFloat XScale = _boxView.frame.origin.x/_cameraView.frame.size.width;
    CGFloat YScale = _boxView.frame.origin.y/_cameraView.frame.size.height;
    captureMetadataOutput.rectOfInterest = CGRectMake(YScale,XScale,1-YScale,1-XScale);
    //    captureMetadataOutput.rectOfInterest = CGRectMake(0.2f, 0.2f, 0.8f, 0.8f);
    NSLog(@"X:%f, Y:%f , W:%f, H:%f", _boxView.frame.origin.x, _boxView.frame.origin.y, _boxView.frame.size.width, _boxView.frame.size.height);
    NSLog(@"X:%f, Y:%f , W:%f, H:%f", _cameraView.frame.origin.x, _cameraView.frame.origin.y, _cameraView.frame.size.width, _cameraView.frame.size.height);
    
    //10.1.扫描框
    _boxView.layer.borderColor = [UIColor greenColor].CGColor;
    _boxView.layer.borderWidth = 1.0f;
    
    [_cameraView addSubview:_boxView];
    [_cameraView addSubview:_bottomView];
    [_cameraView addSubview:_leftView];
    [_cameraView addSubview:_rightView];
    [_cameraView addSubview:_topView];
    
    //10.2.扫描线
    _scanLayer = [[CALayer alloc] init];
    _scanLayer.frame = CGRectMake(0, 0, _boxView.bounds.size.width, 1);
    _scanLayer.backgroundColor = [UIColor brownColor].CGColor;
    
    [_boxView.layer addSublayer:_scanLayer];
    
//    _timer = [NSTimer scheduledTimerWithTimeInterval:10000.0f target:self selector:@selector(moveScanLayer:) userInfo:nil repeats:YES];
//    
//    [_timer fire];
    
    //10.开始扫描
    [_captureSession startRunning];
    
    
    return YES;
}

-(void)stopReading{
    [_captureSession stopRunning];
    _captureSession = nil;
    [_scanLayer removeFromSuperlayer];
//    [_timer invalidate];
//    _timer  = nil;
    [_videoPreviewLayer removeFromSuperlayer];
}

-(void)timerFireMethod//弹出框
{
    
}


#pragma mark - AVCaptureMetadataOutputObjectsDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    //判断是否有数据
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        NSLog(@"stoping Reading");
        [self stopReading];
        AVMetadataMachineReadableCodeObject  *metadataObj = [metadataObjects objectAtIndex:0];
        NSString *qr = [metadataObj stringValue];
        [self performSelectorOnMainThread:@selector(reportScanResult:) withObject:qr waitUntilDone:NO];
    }
}

-(void)reportScanResult:(NSString *)qr{
        NSLog(@"Got Meta");
        
        ResourceDetailViewController *vc = [[ResourceDetailViewController alloc] init];
        vc.qrCode = qr;
        NSLog(@"Prepare to push");
        [self.navigationController pushViewController:vc animated:YES];
        NSLog(@"VC Pushed");
}

- (void)moveScanLayer:(NSTimer *)timer
{
    if (_timer == nil){
        return;
    }
    
    CGRect frame = _scanLayer.frame;
    CGFloat step = _boxView.frame.size.height/25;
    frame.origin.y += step;
    if (_boxView.frame.size.height < _scanLayer.frame.origin.y) {
        frame.origin.y = 0;
        _scanLayer.frame = frame;
    }else{
        [UIView animateWithDuration:0.1 animations:^{
            _scanLayer.frame = frame;
        }];
    }
    NSLog(@"moved");
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)quitBtnClick:(id)sender {
    [self stopReading];
    [self.navigationController popViewControllerAnimated:YES];
    //        [self startReading];
}

@end
