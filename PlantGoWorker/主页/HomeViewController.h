//
//  HomeViewController.h
//  PlantGoWorker
//
//  Created by 纪翀 on 17/4/23.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tabView;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIButton *avatarImgBtn;


@property NSDictionary* pageData;

@end
