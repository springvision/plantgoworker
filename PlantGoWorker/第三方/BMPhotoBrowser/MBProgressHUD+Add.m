//
//  MBProgressHUD+Add.m
//  视频客户端
//
//  Created by mj on 13-4-18.
//  Copyright (c) 2013年 itcast. All rights reserved.
//
// 内容字体
#define KFontOfContent(fontSize) [UIFont fontWithName:@"Arial" size:fontSize]
#import "MBProgressHUD+Add.h"
#define HUDTag 99999
@implementation MBProgressHUD (Add)

#pragma mark 显示信息
+ (void)show:(NSString *)text icon:(NSString *)icon view:(UIView *)view
{
    if (view == nil) view = [UIApplication sharedApplication].keyWindow;
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.labelText = text;
    // 设置图片
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"MBProgressHUD.bundle/%@", icon]]];
    // 再设置模式
    hud.mode = MBProgressHUDModeCustomView;
    
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    
    // 1秒之后再消失
    [hud hide:YES afterDelay:0.7];
}

#pragma mark 显示错误信息
+ (void)showError:(NSString *)error toView:(UIView *)view
{
    [self show:error icon:@"error.png" view:view];
}

+ (void)showSuccess:(NSString *)success toView:(UIView *)view
{
    [self show:success icon:@"success.png" view:view];
}

#pragma mark 显示一些信息
+ (MBProgressHUD *)showMessag:(NSString *)message toView:(UIView *)view {
    if (view == nil) view = [UIApplication sharedApplication].keyWindow;
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.labelText = message;
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    // YES代表需要蒙版效果
    hud.dimBackground = YES;
    return hud;
}
+ (void)ShowWithText:(NSString *)text fromView:(UIView *)view
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    if (view == nil) {
        view = [[UIApplication sharedApplication] keyWindow];
    }
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.labelText = text;
    hud.margin = 10.f;
    //    hud.yOffset = 150.f;
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hide:YES afterDelay:1];
}

+ (void)hidFormView:(UIView *)view
{
    MBProgressHUD *viewHud = (MBProgressHUD *)[view viewWithTag:HUDTag];
    if (viewHud)
    {
        [viewHud hide:YES afterDelay:0.1];
    }
}

+ (void)hidFormView:(UIView *)view afterDelay:(CGFloat)delay
{
    MBProgressHUD *viewHud = (MBProgressHUD *)[view viewWithTag:HUDTag];
    if (viewHud)
    {
        [viewHud hide:YES afterDelay:delay];
    }
}

+ (void)showLoadingToView:(UIView *)view
{
    MBProgressHUD *viewHud = (MBProgressHUD *)[view viewWithTag:HUDTag];
    if (!viewHud)
    {
        MBProgressHUD *hud  = [[MBProgressHUD alloc] initWithView:view];
        hud.tag = HUDTag;
        hud.removeFromSuperViewOnHide = YES;
        [view addSubview:hud];
        //hud.labelText = @"";
        [hud show:YES];
    }
    //	hud.delegate = self;
}

+ (void)showText:(NSString *)text toView:(UIView *)view
{
    if (view == nil) {
        view = [[UIApplication sharedApplication] keyWindow];
    }
    [MBProgressHUD hideAllHUDsForView:view animated:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = text;
        hud.labelFont = KFontOfContent(15);
        hud.margin = 10.f;
        hud.opacity = 1.0;
        hud.minSize = CGSizeMake(120, 40);
        //    hud.dimBackground = YES;
        hud.yOffset = 110.f;
        hud.removeFromSuperViewOnHide = YES;
        
        [hud hide:YES afterDelay:1.0];
    });
}
+ (void)showText:(NSString *)text toView:(UIView *)view finish:(void (^)(void))finish
{
    if (view == nil) {
        view = [[UIApplication sharedApplication] keyWindow];
    }
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = text;
    hud.completionBlock = finish;
    hud.labelFont = KFontOfContent(15);
    hud.margin = 10.f;
    hud.opacity = 1.0;
    hud.minSize = CGSizeMake(120, 40);
    //    hud.dimBackground = YES;
    hud.yOffset = 110.f;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:1.5];
}
+ (void)showLongText:(NSString *)text toView:(UIView *)view
{
    if (view == nil) {
        view = [[UIApplication sharedApplication] keyWindow];
    }
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.detailsLabelText = text;
    hud.detailsLabelFont = KFontOfContent(15);
    hud.margin = 10.f;
    hud.opacity = 1.0;
    hud.minSize = CGSizeMake(120, 40);
    //    hud.dimBackground = YES;
    hud.yOffset = 110.f;
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hide:YES afterDelay:1.5];
}
@end
