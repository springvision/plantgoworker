//
//  ViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 17/4/23.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "ViewController.h"
#import "HomeViewController.h"
#import "DateTool.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSInteger beginWeekDay, endWeekDay;
    
    NSDictionary *dic = [DateTool getMonthBeginAndEndWith:@"2017-04"];
    beginWeekDay = [DateTool getWeekdayFromDate:[dic objectForKey:@"beginDate"]];
    endWeekDay   = [DateTool getWeekdayFromDate:[dic objectForKey:@"endDate"]];
    
    // Do any additional setup after loading the view, typically from a nib.
    // 禁用 iOS7 返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    self.navigationController.navigationBar.hidden = YES;
    
    HomeViewController *vc = [[HomeViewController alloc]init];
    
    NSUserDefaults   *user = [NSUserDefaults standardUserDefaults];
    NSString *username = [user objectForKey:@"phone"];
    NSString *password = [user objectForKey:@"password"];
    NSNumber *ts = [NSNumber numberWithUnsignedInteger:[[NSDate date] timeIntervalSince1970]*1000];

    if (username != nil) {
        NSDictionary *postDic = @{
                                  @"password": password,
                                  @"name": username,
                                  @"ts": ts
                                  };
        [SendRequestClass commonRequest:@"/api/login"
                                postDic:postDic
                        completionBlock:^(NSDictionary *loginDic)  {
                            if ([[loginDic objectForKey:@"code"] intValue] == SUCC_LOGIN){
                                NSLog(@"Success Login");
                                
                                //持久化
                                NSUserDefaults  *user = [NSUserDefaults standardUserDefaults];
                                [user setObject:username forKey:@"phone"];
                                [user setObject:password forKey:@"password"];
                                //存token
                                NSString *token = [loginDic objectForKey:@"data"];
                                [GlobalVar setVarName:@"token" varValue:token];
                                
                                
                                //获取首页
                                ////////////////////////  API  ///////////////////////////
                                [SendRequestClass commonRequest:@"/api/mainpage"
                                                        postDic:@{@"token":token}
                                                completionBlock:^(NSDictionary *mainPageDic)  {
                                                    if ([[mainPageDic objectForKey:@"code"] intValue] == SUCC_RESP){
                                                        NSLog(@"Success MainPage");
                                                        NSDictionary *data = [mainPageDic objectForKey:@"data"];
                                                        
                                                        //传值跳转
                                                        vc.pageData = data;
                                                        [self stopWaiting];
                                                        [self.navigationController pushViewController:vc animated:YES];
                                                    }
                                                    else{
                                                        NSLog(@"Success MainPage");
                                                        [MyHint showHint:[mainPageDic objectForKey:@"message"]];
                                                    }
                                                    
                                                }failureBlock:^(NSDictionary *failDic){
                                                    NSLog(@"Network error");
                                                    [MyHint showHint:@"网络出现故障"];
                                                }
                                 ];
                            }
                            else{
                                NSLog(@"Success fail");
                                [MyHint showHint:[loginDic objectForKey:@"message"]];
                            }
                            
                            
                        }failureBlock:^(NSDictionary *dic){
                            NSLog(@"Network error");
                            [MyHint showHint:@"网络出现故障"];
                        }
         ];
        [self stopWaiting];

    }
        

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    不显示Navigator
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (IBAction)loginBtnClick:(id)sender {
    [self startWaiting];
    
    HomeViewController *vc = [[HomeViewController alloc]init];

    NSString *username = _userText.text;
    NSString *password = _passText.text.md5;
    NSNumber *ts = [NSNumber numberWithUnsignedInteger:[[NSDate date] timeIntervalSince1970]*1000];
    NSDictionary *postDic = @{
               @"password": password,
               @"name": username,
               @"ts": ts
               };
    ////////////////////////  API  ///////////////////////////
    [SendRequestClass commonRequest:@"/api/login"
                    postDic:postDic
                    completionBlock:^(NSDictionary *loginDic)  {
                        if ([[loginDic objectForKey:@"code"] intValue] == SUCC_LOGIN){
                            NSLog(@"Success Login");
                            
                            //持久化
                            NSUserDefaults  *user = [NSUserDefaults standardUserDefaults];
                            [user setObject:username forKey:@"phone"];
                            [user setObject:password forKey:@"password"];
                            //存token
                            NSString *token = [loginDic objectForKey:@"data"];
                            [GlobalVar setVarName:@"token" varValue:token];
                            
                            
                            //获取首页
                            ////////////////////////  API  ///////////////////////////
                            [SendRequestClass commonRequest:@"/api/mainpage"
                                                    postDic:@{@"token":token}
                                            completionBlock:^(NSDictionary *mainPageDic)  {
                                                if ([[mainPageDic objectForKey:@"code"] intValue] == SUCC_RESP){
                                                    NSLog(@"Success MainPage");
                                                    NSDictionary *data = [mainPageDic objectForKey:@"data"];
                                                    
                                                    //传值跳转
                                                    vc.pageData = data;
                                                    [self stopWaiting];
                                                    [self.navigationController pushViewController:vc animated:YES];
                                                }
                                                else{
                                                    NSLog(@"Success MainPage");
                                                    [MyHint showHint:[mainPageDic objectForKey:@"message"]];
                                                }
                                                
                                            }failureBlock:^(NSDictionary *failDic){
                                                NSLog(@"Network error");
                                                [MyHint showHint:@"网络出现故障"];
                                            }
                             ];
                        }
                        else{
                            NSLog(@"Success fail");
                            [MyHint showHint:[loginDic objectForKey:@"message"]];
                        }
                        
                                                                
                    }failureBlock:^(NSDictionary *dic){
                        NSLog(@"Network error");
                        [MyHint showHint:@"网络出现故障"];
                    }
     ];
    [self stopWaiting];
}

@end
