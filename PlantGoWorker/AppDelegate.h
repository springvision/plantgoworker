//
//  AppDelegate.h
//  PlantGoWorker
//
//  Created by 纪翀 on 17/4/23.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

