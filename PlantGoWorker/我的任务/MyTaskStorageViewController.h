//
//  MyTaskStorageViewController.h
//  PlantGoWorker
//
//  Created by 纪翀 on 17/4/24.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTaskStorageViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tabView;

@property NSString * stockId;
@property NSString * storageId;
@property NSString * workDate;
@property (weak, nonatomic) IBOutlet UILabel *finishLabel;
@property (weak, nonatomic) IBOutlet UILabel *unfinishLabel;
@property (weak, nonatomic) IBOutlet UIButton *checkBtn;
@end
