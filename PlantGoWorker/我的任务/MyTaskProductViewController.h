//
//  MyTaskProductViewController.h
//  PlantGoWorker
//
//  Created by 纪翀 on 17/4/24.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTaskProductViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tabView;

@property NSString * storageId;
@property NSString * resConfId;
@property NSString * picUrl;
@property NSString * workDate;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *attrLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *workLabel;

@end
