//
//  MyTaskProductViewCell.h
//  PlantGoWorker
//
//  Created by 纪翀 on 17/4/24.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTaskProductViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *barcodeLabel;

@end
