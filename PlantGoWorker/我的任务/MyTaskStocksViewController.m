//
//  MyTaskStocksViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 17/4/24.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "MyTaskStocksViewController.h"
#import "MyTaskStocksViewCell.h"
#import "MyTaskStorageViewController.h"

@interface MyTaskStocksViewController ()
{
    NSArray *_dataArr;
}
@end

@implementation MyTaskStocksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
    [self initTabView];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)loadData{
    NSString *token = [GlobalVar getVarName:@"token"];
    NSDictionary *postDic = @{
                              @"token":token,
                              @"allDays":_dateBtn.tag == 0 ? @"0":@"1"
                              };
    [SendRequestClass commonRequest:@"/api/mystocktasks" postDic:postDic completionBlock:^(NSDictionary *succDic) {
        if ([[succDic objectForKey:@"code"] intValue] == SUCC_RESP)
        {
            NSLog(@"Success mystocktasks");
            NSDictionary *data = [succDic objectForKey:@"data"];
            _dataArr = [data objectForKey:@"works"];
            double finish = [[data objectForKey:@"finish"] doubleValue]/60;
            double unfinish = [[data objectForKey:@"unfinish"] doubleValue]/60;
            double route = [[data objectForKey:@"traceTime"] doubleValue]/60;
            [_finishLabel setText:[NSString stringWithFormat:@"已%.2f小时", finish ]];
            [_unfinishLabel setText:[NSString stringWithFormat:@"未%.2f小时", unfinish ]];
            [_routeTimeLabel setText:[NSString stringWithFormat:@"路%.2f小时", route ]];
            [_tabView reloadData];
        }
        else{
            NSLog(@"Fail mystocktasks");
            [MyHint showHint:[succDic objectForKey:@"message"]];
        }
        
    } failureBlock:^(NSDictionary *failDic) {
        NSLog(@"Network error");
        [MyHint showHint:@"网络出现故障"];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITabView
- (void)initTabView
{
    _tabView.rowHeight = 70;
//    UIView * view = [[UIView alloc] initWithFrame:CGRectZero];
//    [_tabView setTableHeaderView:view];
    _tabView.bounces = NO;
    [_tabView registerNib:[UINib nibWithNibName:@"MyTaskStocksViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_dataArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *rowDic = [_dataArr objectAtIndex:indexPath.section];
    MyTaskStocksViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell.titleLabel setText:[rowDic objectForKey:@"stockroomName"]];
    NSString *unfinish = [NSString stringWithFormat:@"未:%.2fm", [[rowDic objectForKey:@"unFinishWorkTime"] doubleValue]];
    [cell.abstractLabel setText:unfinish];
    
    NSString *finish = [NSString stringWithFormat:@"已:%.2fm", [[rowDic objectForKey:@"finishWorkTime"]doubleValue]];
    [cell.finishLabel setText:finish];
    
    NSString *route = [NSString stringWithFormat:@"路:%.2fm", [[rowDic objectForKey:@"traceTime"]doubleValue]];
    [cell.routeTimeLabel setText:route];
    
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:[rowDic objectForKey:@"picUrl"]]  placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
    
    cell.tag = [[rowDic objectForKey:@"stockroomId"] intValue];
    
    BOOL workState = [[rowDic objectForKey:@"workState"] boolValue];
    
    if (workState){
        cell.contentView.alpha = 0.5;
        [cell setUserInteractionEnabled:NO];
    }else{
        cell.contentView.alpha = 1;
        [cell setUserInteractionEnabled:YES];
    }
    
    [cell.dateLabel setText:[rowDic objectForKey:@"workDate"]];
    
    return cell;
}

- (UIView *)headerView
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectZero];
    return view;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dic = [_dataArr objectAtIndex:indexPath.section];
    MyTaskStorageViewController *vc = [[MyTaskStorageViewController alloc]init];
    vc.stockId = [dic objectForKey:@"stockroomId"];
    vc.workDate = [dic objectForKey:@"workDate"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)dateBtn:(UIButton*)sender {
    if (sender.tag == 0){
        [sender setTitle:@"周期" forState:UIControlStateNormal];
        sender.tag = 1;
    }else{
        [sender setTitle:@"当天" forState:UIControlStateNormal];
        sender.tag = 0;
    }
    [self loadData];
}



@end
