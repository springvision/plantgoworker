//
//  MyTaskProductViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 17/4/24.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "MyTaskProductViewController.h"
#import "MyTaskProductViewCell.h"

@interface MyTaskProductViewController ()
{
    NSArray *_dataArr;
}
@end

@implementation MyTaskProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_imgView sd_setImageWithURL:[NSURL URLWithString:_picUrl] placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
    [self initTabView];
    [self loadData];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData{
    NSString *token = [GlobalVar getVarName:@"token"];
    NSDictionary *postDic = @{
                              @"token":token,
                              @"resConfId":_resConfId,
                              @"storageId":_storageId,
                              @"workDate":_workDate
                              };
    [SendRequestClass commonRequest:@"/api/storageproducttasks" postDic:postDic completionBlock:^(NSDictionary *succDic) {
        if ([[succDic objectForKey:@"code"] intValue] == SUCC_RESP)
        {
            NSLog(@"Success storageproducttasks");
            NSDictionary *data = [succDic objectForKey:@"data"];
            _dataArr = [data objectForKey:@"resIds"];
            NSDictionary *resConfDic = [data objectForKey:@"resConf"];
            [_nameLabel setText:[resConfDic objectForKey:@"resConfName"]];
            [_titleLabel setText:_nameLabel.text];
            
            [_countLabel setText:[NSString stringWithFormat:@"%lu个", (unsigned long)[_dataArr count]]];
            NSMutableString *attrStr = [NSMutableString stringWithCapacity:200];
            for (NSDictionary *attDic in [resConfDic objectForKey:@"attributes"]){
                [attrStr appendString:[attDic objectForKey:@"name"]];
                [attrStr appendString:@":"];
                [attrStr appendString:[attDic objectForKey:@"text"]];
                [attrStr appendString:@" "];
            }
            [_attrLabel setText:attrStr];
            
            NSMutableString *workStr = [NSMutableString stringWithCapacity:200];
            for (NSString *work in [data objectForKey:@"workDetail"]){
                [workStr appendString:work];
                [workStr appendString:@" "];
            }
            [_workLabel setText:workStr];
            
            
            [_tabView reloadData];
        }
        else{
            NSLog(@"Fail storageproducttasks");
            [MyHint showHint:[succDic objectForKey:@"message"]];
        }
        
    } failureBlock:^(NSDictionary *failDic) {
        NSLog(@"Network error");
        [MyHint showHint:@"网络出现故障"];
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - UITabView
- (void)initTabView
{
    _tabView.rowHeight = 44;
    //    UIView * view = [[UIView alloc] initWithFrame:CGRectZero];
    //    [_tabView setTableHeaderView:view];
    _tabView.bounces = NO;
    [_tabView registerNib:[UINib nibWithNibName:@"MyTaskProductViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_dataArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *barCode = [_dataArr objectAtIndex:indexPath.section];
    MyTaskProductViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell.barcodeLabel setText:barCode];
    return cell;
}

- (UIView *)headerView
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectZero];
    return view;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)finishBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
