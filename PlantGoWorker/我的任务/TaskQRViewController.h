//
//  TaskQRViewController.h
//  HuiJu
//
//  Created by 纪翀 on 16/2/24.
//  Copyright © 2016年 恒宇博基. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskQRViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *cameraView;
@property (weak, nonatomic) IBOutlet UIView *boxView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quitBtn;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *leftView;
@property (weak, nonatomic) IBOutlet UIView *rightView;
@property (weak, nonatomic) IBOutlet UIView *topView;

@property NSString *storageId;

@end
