//
//  MyTaskStorageDetailViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 17/4/24.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "MyTaskStorageDetailViewController.h"
#import "MyTaskStorageDetailViewCell.h"
#import "MyTaskProductViewController.h"
#import "TaskQRViewController.h"
#import "GTMBase64.h"

@interface MyTaskStorageDetailViewController ()
{
    NSArray *_dataArr;
    UIImagePickerController *_picker;
}
@end

@implementation MyTaskStorageDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTabView];
    [self loadData];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData{
    NSString *token = [GlobalVar getVarName:@"token"];
    NSDictionary *postDic = @{
                              @"token":token,
                              @"storageId":_storageId,
                              @"workDate":_workDate
                              };
    [SendRequestClass commonRequest:@"/api/mystoragetaskdetail" postDic:postDic completionBlock:^(NSDictionary *succDic) {
        if ([[succDic objectForKey:@"code"] intValue] == SUCC_RESP)
        {
            NSLog(@"Success mystoragetaskdetail");
            NSDictionary *data = [succDic objectForKey:@"data"];
            _dataArr = [data objectForKey:@"works"];
            NSDictionary *storageDic = [data objectForKey:@"storage"];
 
                [_imgView sd_setImageWithURL:[NSURL URLWithString:[storageDic objectForKey:@"picUrl"]]  placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
            [_storageLabel setText:[storageDic objectForKey:@"name"]];
            [_tabView reloadData];
        }
        else{
            NSLog(@"Fail mystoragetaskdetail");
            [MyHint showHint:[succDic objectForKey:@"message"]];
        }
        
    } failureBlock:^(NSDictionary *failDic) {
        NSLog(@"Network error");
        [MyHint showHint:@"网络出现故障"];
    }];
}



#pragma mark - UITabView
- (void)initTabView
{
    _tabView.rowHeight = 78;
    //    UIView * view = [[UIView alloc] initWithFrame:CGRectZero];
    //    [_tabView setTableHeaderView:view];
    _tabView.bounces = NO;
    [_tabView registerNib:[UINib nibWithNibName:@"MyTaskStorageDetailViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_dataArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *rowDic = [_dataArr objectAtIndex:indexPath.section];
    MyTaskStorageDetailViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    NSMutableString *attrStr = [NSMutableString stringWithCapacity:200];
    for (NSDictionary *attDic in [rowDic objectForKey:@"attributes"]){
        [attrStr appendString:[attDic objectForKey:@"name"]];
        [attrStr appendString:@":"];
        [attrStr appendString:[attDic objectForKey:@"text"]];
        [attrStr appendString:@" "];
    }
    cell.tag = [[rowDic objectForKey:@"resConfId"] integerValue];
    [cell.abstractLabel setText:attrStr];
    [cell.titleLabel setText:[rowDic objectForKey:@"resConfName"]];
    
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:[rowDic objectForKey:@"picUrl"]] placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];

    [cell.countLabel setText:[NSString stringWithFormat:@"%@", [rowDic objectForKey:@"count"]]];
    return cell;
}

- (UIView *)headerView
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectZero];
    return view;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *rowDic = [_dataArr objectAtIndex:indexPath.section];
    
    MyTaskProductViewController *vc = [[MyTaskProductViewController alloc]init];
    vc.resConfId = [rowDic objectForKey:@"resConfId"];
    vc.storageId = _storageId;
    vc.workDate  = _workDate;
    
    [vc.titleLabel setText:[rowDic objectForKey:@"resConfName"]];
    [vc.nameLabel  setText:[rowDic objectForKey:@"resConfName"]];
    
    vc.picUrl = [rowDic objectForKey:@"picUrl"];
    
    NSMutableString *attrStr = [NSMutableString stringWithCapacity:200];
    for (NSDictionary *attDic in [rowDic objectForKey:@"attributes"]){
        [attrStr appendString:[attDic objectForKey:@"name"]];
        [attrStr appendString:@":"];
        [attrStr appendString:[attDic objectForKey:@"text"]];
        [attrStr appendString:@" "];
    }
    [vc.attrLabel setText:attrStr];
    [vc.countLabel setText:[NSString stringWithFormat:@"%@", [rowDic objectForKey:@"count"]]];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)completeBtnClick:(id)sender {
    TaskQRViewController *vc = [[TaskQRViewController alloc]init];
    vc.storageId = _storageId;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
