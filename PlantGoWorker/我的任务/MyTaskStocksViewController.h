//
//  MyTaskStocksViewController.h
//  PlantGoWorker
//
//  Created by 纪翀 on 17/4/24.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTaskStocksViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tabView;
@property (weak, nonatomic) IBOutlet UILabel *finishLabel;
@property (weak, nonatomic) IBOutlet UILabel *unfinishLabel;
@property (weak, nonatomic) IBOutlet UILabel *routeTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *dateBtn;
@end
