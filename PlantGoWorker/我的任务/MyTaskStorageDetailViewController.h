//
//  MyTaskStorageDetailViewController.h
//  PlantGoWorker
//
//  Created by 纪翀 on 17/4/24.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTaskStorageDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tabView;
@property NSString * workDate;
@property NSString* storageId;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *storageLabel;

@end
