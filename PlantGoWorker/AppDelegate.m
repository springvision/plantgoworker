//
//  AppDelegate.m
//  PlantGoWorker
//
//  Created by 纪翀 on 17/4/23.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {

    
    NSUserDefaults   *user = [NSUserDefaults standardUserDefaults];
    NSString *username = [user objectForKey:@"phone"];
    NSString *password = [user objectForKey:@"password"];
    NSNumber *ts = [NSNumber numberWithUnsignedInteger:[[NSDate date] timeIntervalSince1970]*1000];
    
    NSDictionary *postDic = @{
                              @"password": password,
                              @"name": username,
                              @"ts": ts
                              };
    [SendRequestClass commonRequest:@"/api/login"
                            postDic:postDic
                    completionBlock:^(NSDictionary *loginDic)  {
                        if ([[loginDic objectForKey:@"code"] intValue] == SUCC_LOGIN){
                            NSLog(@"Success Login");
                            
                            //存token
                            NSString *token = [loginDic objectForKey:@"data"];
                            [GlobalVar setVarName:@"token" varValue:token];
                            
                        }
                        else{
                            NSLog(@"Success fail");
                            [MyHint showHint:[loginDic objectForKey:@"message"]];
                        }
                        
                    }failureBlock:^(NSDictionary *dic){
                        NSLog(@"Network error");
                        [MyHint showHint:@"网络出现故障"];
                    }
     ];
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
