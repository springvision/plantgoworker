//
//  UIControl+NoDBClick.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/7/11.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIControl (NoDBClick)
@property (nonatomic, assign) NSTimeInterval cs_acceptEventInterval; // 重复点击的间隔

@property (nonatomic, assign) NSTimeInterval cs_acceptEventTime;
@property (nonatomic, assign) NSInteger cs_canDBClick;
@end
