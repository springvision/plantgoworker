//
//  MyWebView.h
//  HuiJu
//
//  Created by 纪翀 on 16/1/7.
//  Copyright © 2016年 恒宇博基. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyWebView : UIWebView

@property int contentType;

-(void)loadContent:(NSDictionary*)listDic detailDic:(NSDictionary*)detailDic;
-(void)adjustLayout;
@end
