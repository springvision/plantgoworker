//
//  GlobalVar.m
//  PlantGoWorker
//
//  Created by 纪翀 on 15/12/2.
//  Copyright © 2015年 青希羽博. All rights reserved.
//

#import "GlobalVar.h"

static NSMutableDictionary *_globalDic = nil;
@implementation GlobalVar


+(void)setVarName:(NSString *)name varValue:(NSObject*)value{
    if (_globalDic == nil){
        _globalDic = [NSMutableDictionary dictionaryWithCapacity:0] ;
    }
    [_globalDic setObject:value forKey:name];
}

+(NSObject*)getVarName:(NSString *)name{
    if (_globalDic == nil){
        _globalDic = [NSMutableDictionary dictionaryWithCapacity:0];
    }
    return [_globalDic objectForKey:name];
}

@end

