//
//  GlobalVar.h
//  PlantGoWorker
//
//  Created by 纪翀 on 15/12/2.
//  Copyright © 2015年 青希羽博. All rights reserved.
//

#ifndef GlobalVar_h
#define GlobalVar_h

@interface GlobalVar : NSObject
+(void)setVarName:(NSString *)name varValue:(NSObject*)value;

+(NSObject*)getVarName:(NSString *)name;

@end


#endif /* GlobalVar_h */
