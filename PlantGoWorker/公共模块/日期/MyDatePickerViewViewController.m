//
//  MyDatePickerViewViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 17/4/24.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "MyDatePickerViewViewController.h"
#import "DateTool.h"
#import "DateItem.h"

@interface MyDatePickerViewViewController ()
@property (nonatomic, assign) NSInteger firstWeekday;
@end

static NSString *dateCell = @"dateCell";

@implementation MyDatePickerViewViewController

- (NSDate *)startDate {
    if (!_startDate) {
        _startDate = [NSDate dateWithTimeIntervalSince1970:0];
    }
    return _startDate;
}


- (NSDate *)endDate {
    if (!_endDate) {
        _endDate = [NSDate dateWithTimeIntervalSinceNow:86400*365*10];
    }
    return _endDate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpDateCollectionView];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpDateCollectionView {
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    CGFloat itemW = (ScreenWidth - 3) / 7;
    flowLayout.itemSize = CGSizeMake(itemW, 45);
    flowLayout.minimumInteritemSpacing = 0.5;
    flowLayout.minimumLineSpacing = 0.5;
    _collectionView.collectionViewLayout = flowLayout;
    _collectionView.bounces = NO;
    _collectionView.showsVerticalScrollIndicator = NO;
    [_collectionView registerNib:[UINib nibWithNibName:@"DateItem" bundle:nil] forCellWithReuseIdentifier:dateCell];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark- UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSInteger days = [DateTool getDaysFromDate:self.startDate toDate:self.endDate];
    return days + 1 + _firstWeekday;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    DateItem *item = [collectionView dequeueReusableCellWithReuseIdentifier:dateCell forIndexPath:indexPath];
        if (indexPath.item >= _firstWeekday) {
            item.date = [DateTool date:self.startDate addDays:indexPath.item - _firstWeekday];
        } else item.date = nil;
        item.isSelect = NO;
        
        return item;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView reloadData];
}




@end
