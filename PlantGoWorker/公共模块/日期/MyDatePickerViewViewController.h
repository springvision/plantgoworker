//
//  MyDatePickerViewViewController.h
//  PlantGoWorker
//
//  Created by 纪翀 on 17/4/24.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyDatePickerViewViewController : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *previousBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UIButton *todayBtn;
@property (weak, nonatomic) IBOutlet UILabel *monthLabel;

/**
 *  日历的开始日期，默认为1970-01-01
 */
@property (nonatomic, strong) NSDate *startDate;


/**
 *  日历的结束日期，默认为当前日期加上10年
 */
@property (nonatomic, strong) NSDate *endDate;


@end

#define ScreenWidth [UIScreen mainScreen].bounds.size.width