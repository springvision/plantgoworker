//
//  UIViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/6/28.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "UIViewController+Indicator.h"

@implementation UIViewController (Indicator)

UIActivityIndicatorView *indicator;

- (void)viewDidLoad{
    
    indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    //设置显示位置
    indicator.center = CGPointMake([[UIScreen mainScreen] bounds].size.width/2, [[UIScreen mainScreen] bounds].size.height/2);
    //将这个控件加到父容器中。
    [self.view addSubview:indicator];
}


- (void)startWaiting{
    [indicator startAnimating];
    self.view.userInteractionEnabled = NO;
}

- (void)stopWaiting{
    [indicator stopAnimating];
    self.view.userInteractionEnabled = YES;
}

@end
