//
//  MyHint.h
//  PlantGoWorker
//
//  Created by 纪翀 on 15/11/19.
//  Copyright © 2015年 青希羽博. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyHint : UIView

- (id)initWithText:(NSString*)text;
- (void) show;
+ (void) showHint:(NSString*) hintText;

@end
