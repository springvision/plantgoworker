//
//  UIViewController.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/6/28.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIViewController (Indicator)

- (void)viewDidLoad;

- (void)startWaiting;

- (void)stopWaiting;
@end
