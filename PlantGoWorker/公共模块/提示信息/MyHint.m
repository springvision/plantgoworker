//
//  MyHint.m
//  PlantGoWorker
//
//  Created by 纪翀 on 15/11/19.
//  Copyright © 2015年 青希羽博. All rights reserved.
//

#import "MyHint.h"

@implementation MyHint

{
    CGFloat label_w;
    CGFloat label_x;
    CGFloat label_y;
    CGFloat label_h;
    
    UILabel         *_textLabel;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithText:(NSString*)text
{
    
    label_w = [[UIScreen mainScreen] bounds].size.width/6*4;  //宽度为2/3屏宽
    label_x = ([[UIScreen mainScreen] bounds].size.width - label_w)/2;
    label_y = ([[UIScreen mainScreen] bounds].size.height*0.8); //在屏高9/10处提示
    label_h = 15;
    
    self= [self initWithFrame:CGRectMake(label_x, label_y, label_w, label_h) text:text];
   
    return self;
}

- (id)initWithFrame:(CGRect)frame text:(NSString*)text
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent: 0.75f];
        self.layer.cornerRadius = 5.0f;
        _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _textLabel.numberOfLines = 0;
        _textLabel.textColor = [UIColor whiteColor];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.backgroundColor = [UIColor clearColor];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.font = [UIFont systemFontOfSize:label_h-4];
         [_textLabel setText:text];
        [self addSubview:_textLabel];
    }
    [UIView animateWithDuration:5.0
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.alpha = 0;
                     }
                     completion:^(BOOL finished){
                             [self removeFromSuperview];
                         }
                         
     ];
    return self;
}

- (void) show
{
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    [window addSubview:self];
}

+ (void) showHint:(NSString*) hintText{
    [[[MyHint alloc]initWithText:hintText]show];
}

@end
