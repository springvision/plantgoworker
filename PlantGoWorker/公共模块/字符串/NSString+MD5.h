//
//  NSString+MD5.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/6/28.
//  Copyright © 2017年 m-training. All rights reserved.
//

#ifndef NSString_MD5_h
#define NSString_MD5_h

#import<Foundation/Foundation.h>
@interface NSString (md5)

- (NSString *) md5;

@end


#endif /* NSString_MD5_h */
