//
//  ResourceDetailViewController.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/8/18.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResourceDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tabView;
@property NSString *qrCode;

@end
