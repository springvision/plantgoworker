//
//  ResourceDetailTableViewCell.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/8/18.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResourceDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end
