//
//  ResourceDetailViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/8/18.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "ResourceDetailViewController.h"
#import "ResourceDetailTableViewCell.h"

@interface ResourceDetailViewController ()
{
    NSArray *_resArray;
    NSDictionary *_storageDic;
    NSDictionary *_stockDic;
    NSDictionary *_productDic;
    NSDictionary *_resourceDic;
}
@end

@implementation ResourceDetailViewController

- (void)viewDidLoad {
    NSLog(@"ResourceDetailViewController did load begin");
    [super viewDidLoad];
    [self initTabView];
    NSLog(@"Load Data");
    [self loadData];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData{
    NSString *token = [GlobalVar getVarName:@"token"];
    
    [SendRequestClass commonRequest:@"/api/resourcedetail" postDic:@{@"token":token, @"resId":_qrCode} completionBlock:^(NSDictionary *succDic) {
        if ([[succDic objectForKey:@"code"] intValue] == SUCC_RESP){
            NSLog(@"Success resourcedetail");
            NSDictionary *dataDic = [succDic objectForKey:@"data"];
            _resArray = [dataDic objectForKey:@"relaResConfs"];
            _productDic = [dataDic objectForKey:@"resConf"];
            _stockDic   = [dataDic objectForKey:@"stockroom"];
            _storageDic = [dataDic objectForKey:@"storage"];
            _resourceDic = [dataDic objectForKey:@"resource"];
            [_tabView reloadData];
        }else{
            NSLog(@"Fail MyInfo");
            [MyHint showHint:[succDic objectForKey:@"message"]];
        }
    } failureBlock:^(NSDictionary *failDic) {
        NSLog(@"Network error");
        [MyHint showHint:@"网络出现故障"];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - UITabView
- (void)initTabView
{
    //    UIView * view = [[UIView alloc] initWithFrame:CGRectZero];
    //    [_tabView setTableHeaderView:view];
    _tabView.bounces = NO;
    [_tabView registerNib:[UINib nibWithNibName:@"ResourceDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    _tabView.estimatedRowHeight = 36;  //  随便设个不那么离谱的值
    _tabView.rowHeight = UITableViewAutomaticDimension;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 5;
        
        default:
            return 4;
            break;
    }
    return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_resArray count] + 1;
}

- (UITableViewCell *)mainSectionView:(UITableView *)tableView numberOfRows:(NSInteger)row{
    ResourceDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    switch (row) {
        case 0:
            cell.nameLabel.text = @"客  户";
            cell.contentLabel.text = [_stockDic objectForKey:@"name"];
            break;
        
        case 1:
            cell.nameLabel.text = @"库  位";
            cell.contentLabel.text = [_storageDic objectForKey:@"name"];
            break;
            
        case 2:
            cell.nameLabel.text = @"名  称";
            cell.contentLabel.text = [_productDic objectForKey:@"resConfName"];
            break;
            
        case 3:
            cell.nameLabel.text = @"条  码";
            cell.contentLabel.text = [_resourceDic objectForKey:@"id"];
            break;
            
        case 4:{
            cell.nameLabel.text = @"属  性";
            NSMutableString *attrStr = [NSMutableString stringWithCapacity:200];
            for (NSDictionary *attDic in [_productDic objectForKey:@"attributes"]){
                [attrStr appendString:[attDic objectForKey:@"name"]];
                [attrStr appendString:@":"];
                [attrStr appendString:[attDic objectForKey:@"text"]];
                [attrStr appendString:@"\n"];
            }
            cell.contentLabel.text = attrStr;
            break;
        }
        default:
            break;
    }
    return cell;
}


- (UITableViewCell *)resSectionView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ResourceDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    NSDictionary *resDic = _resArray[indexPath.section -1];
    switch (indexPath.row) {
        case 0:
            cell.nameLabel.text = @"名  称";
            cell.contentLabel.text = [resDic objectForKey:@"resConfName"];
            break;
      
        case 1:
            cell.nameLabel.text = @"类  型";
            cell.contentLabel.text = [resDic objectForKey:@"varietyName"];
            break;
   
        case 2:
            cell.nameLabel.text = @"数  量";
            cell.contentLabel.text = [NSString stringWithFormat:@"%@", [resDic objectForKey:@"count"]];
            break;
    
        case 3:{
            cell.nameLabel.text = @"属  性";
            NSMutableString *attrStr = [NSMutableString stringWithCapacity:200];
            for (NSDictionary *attDic in [resDic objectForKey:@"attributes"]){
                [attrStr appendString:[attDic objectForKey:@"name"]];
                [attrStr appendString:@":"];
                [attrStr appendString:[attDic objectForKey:@"text"]];
                [attrStr appendString:@"\n"];
            }
            cell.contentLabel.text = attrStr;
            break;
        }
        default:
            break;
    }
    
    return cell;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell;
    switch (indexPath.section) {
        case 0:
            cell = [self mainSectionView:tableView numberOfRows:indexPath.row];
            break;
            
        default:
            cell = [self resSectionView:tableView cellForRowAtIndexPath:indexPath ];
            break;
    }
    
    return cell;
}

- (UIView *)headerView
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectZero];
    return view;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
