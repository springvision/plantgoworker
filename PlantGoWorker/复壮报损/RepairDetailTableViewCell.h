//
//  RepairDetailTableViewCell.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/9/25.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RepairDetailTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@end
