//
//  RepairDetailViewController.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/9/25.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RepairDetailViewController : UIViewController

@property (strong, nonatomic) NSString *repairId;
@property (weak, nonatomic) IBOutlet UITableView *tabView;

@property (weak, nonatomic) IBOutlet UILabel *barCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *resConfLabel;
@property (weak, nonatomic) IBOutlet UILabel *stockLabel;
@property (weak, nonatomic) IBOutlet UILabel *storageLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@property (weak, nonatomic) NSString *type;

@end
