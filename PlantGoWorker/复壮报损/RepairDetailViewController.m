//
//  RepairDetailViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/9/25.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "RepairDetailViewController.h"
#import "RepairDetailTableViewCell.h"
#import "MJPhoto.h"
#import "MJPhotoBrowser.h"

@interface RepairDetailViewController ()
{
    NSArray *_dataArr;
}
@end

@implementation RepairDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTabView];
    [self loadData];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadData{
    NSString *token = [GlobalVar getVarName:@"token"];
    NSDictionary *postDic = @{
                              @"type":[@"0" isEqualToString:_type] ? @0 : @1,//0为复壮1为报损
                              @"repairId": _repairId,
                              @"token":token
                              };
    [SendRequestClass commonRequest:@"/api/myrepairlist" postDic:postDic completionBlock:^(NSDictionary *succDic) {
        if ([[succDic objectForKey:@"code"] intValue] == SUCC_RESP)
        {
            NSLog(@"Success MyOffTimeList");
            NSArray *retArr = [succDic objectForKey:@"data"];
            if ([retArr count] == 1){
                NSDictionary *data = [retArr objectAtIndex:0];
                _dataArr = [data objectForKey:@"picUrls"];
                NSString *resId = [data objectForKey:@"resId"];
                NSString *resConfName = [data objectForKey:@"resConfName"];
                NSString *stockroomName = [data objectForKey:@"stockroomName"] ? [data objectForKey:@"stockroomName"] : @"";
                NSString *storageAddress = [data objectForKey:@"storageAddress"] ? [data objectForKey:@"storageAddress"] : @"";
                NSString *desc = [data objectForKey:@"desc"] ? [data objectForKey:@"desc"] : @"";
                [_barCodeLabel setText:[@"编码: " stringByAppendingString:resId]];
                [_resConfLabel setText:[@"品种: " stringByAppendingString:resConfName]];
                [_stockLabel   setText:[@"品种: " stringByAppendingString:stockroomName]];
                [_storageLabel setText:[@"仓库: " stringByAppendingString:storageAddress]];
                [_descLabel setText:[@"描述: " stringByAppendingString:desc]];
                [_tabView reloadData];
            }
            else{
                [MyHint showHint:@"没有数据或数据不唯一"];
            }
            
        }
        else{
            NSLog(@"Fail MyOffTimeList");
            [MyHint showHint:[succDic objectForKey:@"message"]];
        }
        
    } failureBlock:^(NSDictionary *failDic) {
        NSLog(@"Network error");
        [MyHint showHint:@"网络出现故障"];
    }];
}

- (void)initTabView
{
    _tabView.estimatedRowHeight = 120;  //  随便设个不那么离谱的值
    _tabView.rowHeight = UITableViewAutomaticDimension;
    [_tabView registerNib:[UINib nibWithNibName:@"RepairDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataArr count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *picUrl = [_dataArr objectAtIndex:indexPath.row];
    RepairDetailTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:picUrl]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray * photos = [[NSMutableArray alloc] init];
    for (int i = 0; i < [_dataArr count]; i ++ )
    {
        MJPhoto * photo = [[MJPhoto alloc] init];
        
        NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[_dataArr objectAtIndex:i]]];
        photo.image = [UIImage imageWithData:data];
        photo.url = [NSURL URLWithString:[_dataArr objectAtIndex:i]];
        [photos addObject:photo];
    }
    //显示
    MJPhotoBrowser *browser = [[MJPhotoBrowser alloc] init];
    browser.photos          = photos;
    browser.currentPhotoIndex = indexPath.row;
    
    [browser show];
}

- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
