//
//  RepairTableViewCell.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/7/7.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RepairTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *barLabel;

@property (weak, nonatomic) IBOutlet UILabel *storageLabel;
@property (weak, nonatomic) IBOutlet UILabel *stockLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;


@end
