//
//  RepairListViewController.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/7/7.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RepairListViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tabView;
@property (weak, nonatomic) NSString *type;
@end
