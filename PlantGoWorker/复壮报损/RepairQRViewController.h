//
//  RepairQRViewController.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/9/25.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RepairQRViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *cameraView;
@property (weak, nonatomic) IBOutlet UIView *boxView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quitBtn;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *leftView;
@property (weak, nonatomic) IBOutlet UIView *rightView;
@property (weak, nonatomic) IBOutlet UIView *topView;

@property (weak, nonatomic) NSString *type;

@end
