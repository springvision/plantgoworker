//
//  RepairListViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/7/7.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "RepairListViewController.h"
#import "RepairTableViewCell.h"
#import "RepairDetailViewController.h"
#import "RepairQRViewController.h"

@interface RepairListViewController ()
{
    NSArray *_dataArr;
}
@end


@implementation RepairListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTabView];
    [self loadData];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadData{
    NSString *token = [GlobalVar getVarName:@"token"];
    NSDictionary *postDic = @{
                              @"type":[@"0" isEqualToString:_type] ? @0 : @1,//0为复壮1为报损
//                              @"repairId": "1234567890",//单子id，当查询列表的时候，不需要穿，查具体单子的时候，可以传
                              @"token":token
                              };
    [SendRequestClass commonRequest:@"/api/myrepairlist" postDic:postDic completionBlock:^(NSDictionary *succDic) {
        if ([[succDic objectForKey:@"code"] intValue] == SUCC_RESP)
        {
            NSLog(@"Success MyOffTimeList");
            _dataArr = [succDic objectForKey:@"data"];
            [_tabView reloadData];
        }
        else{
            NSLog(@"Fail MyOffTimeList");
            [MyHint showHint:[succDic objectForKey:@"message"]];
        }
        
    } failureBlock:^(NSDictionary *failDic) {
        NSLog(@"Network error");
        [MyHint showHint:@"网络出现故障"];
    }];
}

- (void)initTabView
{
    _tabView.estimatedRowHeight = 120;  //  随便设个不那么离谱的值
    _tabView.rowHeight = UITableViewAutomaticDimension;
    [_tabView registerNib:[UINib nibWithNibName:@"RepairTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataArr count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *rowDic = [_dataArr objectAtIndex:indexPath.row];
    RepairTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell.storageLabel setText:[rowDic objectForKey:@"resConfName"]];
    [cell.stockLabel setText:[rowDic objectForKey:@"stockroomName"]];
    NSString *desc = [rowDic objectForKey:@"desc"];
    if (![desc isEqual:[NSNull null]]){
        [cell.descLabel setText:desc];
    }
    [cell.dateLabel setText:[rowDic objectForKey:@"repairDate"]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *row = [_dataArr objectAtIndex:indexPath.row];
    RepairDetailViewController *vc = [[RepairDetailViewController alloc] init];
    vc.type = _type;
    vc.repairId = [row objectForKey:@"id"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)applyBtnClick:(id)sender {
    RepairQRViewController *vc =
    [[RepairQRViewController alloc]init];
    vc.type = _type;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
