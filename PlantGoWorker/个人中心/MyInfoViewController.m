//
//  MyInfoViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/6/28.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "MyInfoViewController.h"
#import "MyInfoTableViewCell.h"
#import "ViewController.h"

@interface MyInfoViewController ()

@end

@implementation MyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initFuncTabView];
    [_imgView sd_setImageWithURL:[NSURL URLWithString:[_myInfoDic objectForKey:@"picUrl"]]];
    [_nameLabel setText:[_myInfoDic objectForKey:@"name"]];
    [_abstractLabel setText:[_myInfoDic objectForKey:@"roleName"]];
    NSDictionary *bundleDic = [[NSBundle mainBundle] infoDictionary];
    NSString *version = [bundleDic objectForKey:@"CFBundleShortVersionString"];
    NSString *appname = [bundleDic objectForKey:@"CFBundleDisplayName"];
    NSString *build   = [bundleDic objectForKey:@"CFBundleVersion"];
    _appInfoLabel.text =[NSString stringWithFormat:@"%@ %@(%@)", appname, version, build];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#define MENU_ARRAY @[\
                       @{@"title":@"退出登录", @"icon":@""}\
                       ]

//@{@"title":@"下载新版本", @"icon":@""},\

#pragma mark - UITabView
- (void)initFuncTabView
{
    _tabView.rowHeight = 70;
    _tabView.bounces = NO;
    [_tabView registerNib:[UINib nibWithNibName:@"MyInfoTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [MENU_ARRAY count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *menu = [MENU_ARRAY objectAtIndex:indexPath.row];
    NSString *name = [menu objectForKey:@"title"];
    
    MyInfoTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell.titleLabel setText:name];
    
    //图标
    [cell.imgView setImage:[UIImage imageNamed:[menu objectForKey:@"icon"]]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:{
            for (UIViewController *start in self.navigationController.viewControllers) {
                if ([start isKindOfClass:[ViewController class]]) {
                    //弹出
                    [start.navigationController popToViewController:start animated:YES];
                }
            }
            break;
        }
        default:
            break;
    }
}

- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
