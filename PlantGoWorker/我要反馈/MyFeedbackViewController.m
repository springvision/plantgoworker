//
//  MyFeedbackViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/6/30.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "MyFeedbackViewController.h"
#import "FeedBackCell.h"
#import "FeedBackDetailViewController.h"
#import "FeedBackReadViewController.h"

@interface MyFeedbackViewController ()
{
    NSArray *_dataArr;
    int _limit;
}
@end

@implementation MyFeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTabView];
    [self loadData];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [self loadData];
}

- (void)initTabView
{
    _tabView.rowHeight = 70;
    _limit = 0;
    [_tabView registerNib:[UINib nibWithNibName:@"FeedBackCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    _tabView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self loadData];
        [_tabView.mj_header endRefreshing];
    }];
    
    _tabView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        _limit += 20;
        [self loadData];
        [_tabView.mj_footer endRefreshing];
    }];
    
}

- (void)loadData{
    NSString *token = [GlobalVar getVarName:@"token"];
    NSDictionary *postDic = @{
                              @"token":token,
                              @"begin":@0,
                              @"limit":[NSNumber numberWithInt:_limit]
                              };
    [SendRequestClass commonRequest:@"/api/myfeedbacks" postDic:postDic completionBlock:^(NSDictionary *succDic) {
        if ([[succDic objectForKey:@"code"] intValue] == SUCC_RESP)
        {
            NSLog(@"Success MyOffTimeList");
            _dataArr = [succDic objectForKey:@"data"];
            [_tabView reloadData];
        }
        else{
            NSLog(@"Fail MyOffTimeList");
            [MyHint showHint:[succDic objectForKey:@"message"]];
        }
        
    } failureBlock:^(NSDictionary *failDic) {
        NSLog(@"Network error");
        [MyHint showHint:@"网络出现故障"];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataArr count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellData = [_dataArr objectAtIndex:indexPath.row];
    
    FeedBackCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell.titleLabel setText:[cellData objectForKey:@"title"]];
    [cell.abstractLabel setText:[cellData objectForKey:@"content"]];
    [cell.dateLabel   setText:[cellData objectForKey:@"feedbackDate"]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *cellData = [_dataArr objectAtIndex:indexPath.row];
    FeedBackReadViewController *vc = [[FeedBackReadViewController alloc] init];
    vc.dataDic = cellData;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)applyBtnClick:(id)sender {
    FeedbackDetailViewController *vc = [[FeedbackDetailViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
