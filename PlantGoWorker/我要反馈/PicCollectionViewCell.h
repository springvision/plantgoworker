//
//  PicCollectionViewCell.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/7/22.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PicCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@end
