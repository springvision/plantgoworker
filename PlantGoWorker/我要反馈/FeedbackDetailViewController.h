//
//  FeedbackDetailViewController.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/6/30.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *addPhotoBtn;
@property (weak, nonatomic) IBOutlet UITextField *titleEdit;
@property (weak, nonatomic) IBOutlet UITextView *contentField;
@property (weak, nonatomic) IBOutlet UITextField *typeEdit;
@property (weak, nonatomic) IBOutlet UIButton *typeSelBtn;

@end
