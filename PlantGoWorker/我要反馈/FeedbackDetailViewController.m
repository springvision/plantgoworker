//
//  FeedbackDetailViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/6/30.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "FeedbackDetailViewController.h"
#import "GTMBase64.h"



@interface FeedbackDetailViewController ()
{
    UIImagePickerController *_picker;
    NSArray *_typeArr;
    NSString *_selType;
}
@end

@implementation FeedbackDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initImagePicker];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initImagePicker{
    _picker = [[UIImagePickerController alloc] init];
    
    // 设置代理
    _picker.delegate = self;
    
    // 设置是否需要做图片编辑，default NO
    _picker.allowsEditing = YES;
}

#pragma mark - ImagePicker
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    // 退出当前界面
    [picker dismissViewControllerAnimated:YES completion:nil];
}

// 选择完成
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    [_addPhotoBtn setBackgroundImage:[info objectForKey:UIImagePickerControllerOriginalImage] forState:UIControlStateNormal];

    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)photoBtnClick:(id)sender {
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
//        _picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        _picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:_picker animated:YES completion:nil];
    }
}

- (IBAction)commitBtnClick:(id)sender {
    NSString  *imageStr = [GTMBase64 stringByEncodingData:UIImageJPEGRepresentation(_addPhotoBtn.currentBackgroundImage, 0.1)];
    
    NSString *token = [GlobalVar getVarName:@"token"];
    if (_selType == nil){
        [MyHint showHint:@"请选择类型"];
        return;
    }
    NSDictionary *postDic = @{
                              @"token":token,
                              @"title":_titleEdit.text,
                              @"content":_contentField.text,
                              @"type":_selType,
                              @"picCode":@[imageStr]
                              };
    [SendRequestClass commonRequest:@"/api/commitfeedback" postDic:postDic completionBlock:^(NSDictionary *succDic) {
        if ([[succDic objectForKey:@"code"] intValue] == SUCC_RESP)
        {
            NSLog(@"Success MyOffTimeList");
            [MyHint showHint:@"提交成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
            NSLog(@"Fail MyOffTimeList");
            [MyHint showHint:[succDic objectForKey:@"message"]];
        }
        
    } failureBlock:^(NSDictionary *failDic) {
        NSLog(@"Network error");
        [MyHint showHint:@"网络出现故障"];
    }];
}


- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showTypeSelection{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"请选择一个反馈类型" message:nil preferredStyle: UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:cancelAction];
    
    for (NSDictionary *row in _typeArr){
        UIAlertAction *selAction = [UIAlertAction actionWithTitle:[row objectForKey:@"name"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            _selType = [row objectForKey:@"value"];
            [_typeEdit setText:[row objectForKey:@"name"]];
        }];
        [alertController addAction:selAction];
    }
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)typeBtnClick:(id)sender {
    NSString *token = [GlobalVar getVarName:@"token"];
    NSDictionary *postDic = @{
                              @"token":token
                              };
    [SendRequestClass commonRequest:@"/api/feedbacktype" postDic:postDic completionBlock:^(NSDictionary *succDic) {
        if ([[succDic objectForKey:@"code"] intValue] == SUCC_RESP)
        {
            _typeArr = [succDic objectForKey:@"data"];
            [self showTypeSelection];
        }
        else{
            NSLog(@"Fail MyOffTimeList");
            [MyHint showHint:[succDic objectForKey:@"message"]];
        }
        
    } failureBlock:^(NSDictionary *failDic) {
        NSLog(@"Network error");
        [MyHint showHint:@"网络出现故障"];
    }];
}

@end
