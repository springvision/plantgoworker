//
//  FeedBackReadViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/7/6.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "FeedBackReadViewController.h"
#import "PicCollectionViewCell.h"
#import "MJPhotoBrowser.h"
#import "MJPhoto.h"

#import "FeedbackReplyCell.h"

@interface FeedBackReadViewController ()
{
    NSArray *_feedbackArr;
}
@end

@implementation FeedBackReadViewController


- (void)initTabView
{
    _tabView.rowHeight = 80;
    [_tabView registerNib:[UINib nibWithNibName:@"FeedbackReplyCell" bundle:nil] forCellReuseIdentifier:@"replyCell"];
    
}

- (void)loadData{
    NSString *token = [GlobalVar getVarName:@"token"];
    NSString *feedbackId = [_dataDic objectForKey:@"feedbackId"];
    NSDictionary *postDic = @{
                              @"token":token,
                              @"feedbackId":feedbackId
                              };
    [SendRequestClass commonRequest:@"/api/myfeedback" postDic:postDic completionBlock:^(NSDictionary *succDic) {
        if ([[succDic objectForKey:@"code"] intValue] == SUCC_RESP)
        {
            NSLog(@"Success MyOffTimeList");
            _feedbackArr = [[succDic objectForKey:@"data"] objectForKey:@"detailInfo"];
            [_tabView reloadData];
        }
        else{
            NSLog(@"Fail MyOffTimeList");
            [MyHint showHint:[succDic objectForKey:@"message"]];
        }
        
    } failureBlock:^(NSDictionary *failDic) {
        NSLog(@"Network error");
        [MyHint showHint:@"网络出现故障"];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [_collectionView registerNib:[UINib nibWithNibName:@"PicCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    UICollectionViewFlowLayout *flowLayout = [_collectionView collectionViewLayout];
    [flowLayout setMinimumLineSpacing:0];
    [flowLayout setMinimumInteritemSpacing:0];
    [self initView];
    [self loadData];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initView{
    [_titleLabel   setText:[_dataDic objectForKey:@"title"]];
    [_contentLabel setText:[_dataDic objectForKey:@"content"]];
    CGSize titleLabelSize = [_titleLabel.text boundingRectWithSize:CGSizeMake(_titleLabel.frame.size.width, 1200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{} context:nil].size;
    _titleHeight.constant = titleLabelSize.height+24;
    CGSize contentLabelSize = [_contentLabel.text boundingRectWithSize:CGSizeMake(_contentLabel.frame.size.width, 1200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{} context:nil].size;
    _contentHeight.constant = contentLabelSize.height+40;
    if ([_dataDic objectForKey:@"picUrl"] && [[_dataDic objectForKey:@"picUrl"] count] > 0 ){
        [_photoImgView sd_setImageWithURL:[NSURL URLWithString:[[_dataDic objectForKey:@"picUrl"] objectAtIndex:0]]];
    }
    [self initTabView];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark --- <UICollectionView>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    CGSize size={0,0};
    return size;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [[_dataDic objectForKey:@"picUrl"] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    PicCollectionViewCell *cell =[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    NSString *picUrl = [[_dataDic objectForKey:@"picUrl"] objectAtIndex:indexPath.row];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:picUrl]];
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray * photos = [[NSMutableArray alloc] init];
    NSArray *picArray = [_dataDic objectForKey:@"picUrl"];
    for (int i = 0; i < [picArray count]; i ++ )
    {
        MJPhoto * photo = [[MJPhoto alloc] init];
        NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[picArray objectAtIndex:i]]];
        
        photo.image = [UIImage imageWithData:data];
        photo.url = [NSURL URLWithString:[picArray objectAtIndex:i]];
        [photos addObject:photo];
    }
    //显示
    MJPhotoBrowser *browser = [[MJPhotoBrowser alloc] init];
    browser.photos          = photos;
    browser.currentPhotoIndex = indexPath.row;
    
    [browser show];
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float cellw =[UIScreen mainScreen].bounds.size.width/3;
    return CGSizeMake(cellw, cellw);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_feedbackArr count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellData = [_feedbackArr objectAtIndex:indexPath.row];
    
    FeedbackReplyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"replyCell"];
    [cell.fromLabel setText:[NSString stringWithFormat:@"派发人：%@", [cellData objectForKey:@"dispatcher"]]];
    [cell.toLabel   setText:[NSString stringWithFormat:@"处理人：%@", [cellData objectForKey:@"dealworker"]]];
    [cell.timeLabel setText:[NSString stringWithFormat:@"创建时间：%@", [cellData objectForKey:@"createDate"]]];
    [cell.statusLabel setText:[NSString stringWithFormat:@"状态：%@", [cellData objectForKey:@"state"]]];
    [cell.contentLabel setText:[NSString stringWithFormat:@"内容：%@", [cellData objectForKey:@"desc"]]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}



- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
