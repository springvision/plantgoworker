//
//  FeedBackCell.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/6/30.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedBackCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *abstractLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
