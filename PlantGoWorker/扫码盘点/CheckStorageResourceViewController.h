//
//  CheckStorageResourceViewController.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/11/4.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckStorageResourceViewController : UIViewController

@property NSString * storageId;
@property NSString * resConfId;
@property NSString * name;
@property NSString * picUrl;
@property (weak, nonatomic) IBOutlet UITableView *tabView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *attrLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@end
