//
//  QRTableViewCell.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/7/1.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QRTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *QRLabel;

@end
