//
//  CheckStorageDetailViewController.h
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/7/1.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckStorageDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tabView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property NSString * storageId;
@end
