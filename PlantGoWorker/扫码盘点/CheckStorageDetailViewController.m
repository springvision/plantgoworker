//
//  CheckStorageDetailViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/7/1.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "CheckStorageDetailViewController.h"
#import "CheckProductCell.h"
#import "QRViewController.h"
#import "CheckStorageResourceViewController.h"

@interface CheckStorageDetailViewController ()
{
    int _limit;
    NSArray *_dataArr;
    NSDictionary *_detailDic;
}
@end

@implementation CheckStorageDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTabView];
    [self loadData];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initTabView
{
    _tabView.rowHeight = 70;
    
    _limit = 0;
    [_tabView registerNib:[UINib nibWithNibName:@"CheckProductCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    _tabView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self loadData];
        [_tabView.mj_header endRefreshing];
    }];
    
    _tabView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        _limit += 20;
        [self loadData];
        [_tabView.mj_footer endRefreshing];
    }];
    
}

- (void)loadData{
    NSString *token = [GlobalVar getVarName:@"token"];
    NSDictionary *postDic = @{
                              @"token":token,
                              @"storageId":_storageId
                              };
    [SendRequestClass commonRequest:@"/api/storagedetail" postDic:postDic completionBlock:^(NSDictionary *succDic) {
        if ([[succDic objectForKey:@"code"] intValue] == SUCC_RESP)
        {
            NSLog(@"Success storagedetail");
            _dataArr = [[succDic objectForKey:@"data"] objectForKey:@"summary"];
            _detailDic =[[succDic objectForKey:@"data"] objectForKey:@"storage"];
            [_titleLabel setText:[_detailDic objectForKey:@"name"]];
            [_tabView reloadData];
        }
        else{
            NSLog(@"Fail storagedetail");
            [MyHint showHint:[succDic objectForKey:@"message"]];
        }
        
    } failureBlock:^(NSDictionary *failDic) {
        NSLog(@"Network error");
        [MyHint showHint:@"网络出现故障"];
    }];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    NSLog(@"begin");
    return YES;
}

-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    NSLog(@"end");
    [self loadData];
    return  YES;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataArr count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellData = [_dataArr objectAtIndex:indexPath.row];
    
    CheckProductCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    NSString *title = [NSString stringWithFormat:@"%@ %@",[cellData objectForKey:@"resType"], [cellData objectForKey:@"resConfName"]];
    [cell.titleLabel setText:title];
    [cell.numLabel setText:[NSString stringWithFormat:@"%@",[cellData objectForKey:@"count"]]];
    
    
    [cell.imgView sd_setImageWithURL:[cellData objectForKey:@"picUrl"] placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
    
    
    NSMutableString *attrStr = [NSMutableString stringWithCapacity:200];
    for (NSDictionary *attDic in [cellData objectForKey:@"attributes"]){
        [attrStr appendString:[attDic objectForKey:@"name"]];
        [attrStr appendString:@":"];
        [attrStr appendString:[attDic objectForKey:@"text"]];
        [attrStr appendString:@" "];
    }
    cell.tag = [[cellData objectForKey:@"resConfId"] integerValue];
    [cell.attrLabel setText:attrStr];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CheckStorageResourceViewController *vc = [[CheckStorageResourceViewController alloc]init];
    NSDictionary *row = [_dataArr objectAtIndex:indexPath.row];
    vc.storageId = _storageId;
    vc.resConfId = [row objectForKey:@"resConfId"];
    vc.name = [row objectForKey:@"resConfName"];
    vc.picUrl = [row objectForKey:@"picUrl"];
    [self.navigationController pushViewController:vc animated:YES];
    
}
- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)scanBtnClick:(id)sender {
    
    QRViewController *vc = [[QRViewController alloc]init];
    vc.storageId = _storageId;
    vc.stockId = [_detailDic objectForKey:@"stockroomId"];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
