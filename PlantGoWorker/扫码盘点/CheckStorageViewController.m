//
//  CheckStorageViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/7/1.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "CheckStorageViewController.h"
#import "MyTaskStorageViewCell.h"
#import "QRViewController.h"
#import "CheckStorageDetailViewController.h"

@interface CheckStorageViewController ()
{
    int _limit;
    NSArray *_dataArr;
}
@end

@implementation CheckStorageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTabView];
    [self loadData];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initTabView
{
    _tabView.rowHeight = 70;
    
    _limit = 0;
    [_tabView registerNib:[UINib nibWithNibName:@"MyTaskStorageViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    _tabView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self loadData];
        [_tabView.mj_header endRefreshing];
    }];
    
    _tabView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        _limit += 20;
        [self loadData];
        [_tabView.mj_footer endRefreshing];
    }];
    
}

- (void)loadData{
    NSString *token = [GlobalVar getVarName:@"token"];
    NSDictionary *postDic = @{
                              @"token":token,
                              @"begin":@0,
                              @"limit":[NSNumber numberWithInt:_limit],
                              @"stockroomId":_stockId
                              };
    [SendRequestClass commonRequest:@"/api/searchstorages" postDic:postDic completionBlock:^(NSDictionary *succDic) {
        if ([[succDic objectForKey:@"code"] intValue] == SUCC_RESP)
        {
            NSLog(@"Success MyOffTimeList");
            _dataArr = [succDic objectForKey:@"data"];
            [_tabView reloadData];
        }
        else{
            NSLog(@"Fail MyOffTimeList");
            [MyHint showHint:[succDic objectForKey:@"message"]];
        }
        
    } failureBlock:^(NSDictionary *failDic) {
        NSLog(@"Network error");
        [MyHint showHint:@"网络出现故障"];
    }];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    NSLog(@"begin");
    return YES;
}

-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    NSLog(@"end");
    [self loadData];
    return  YES;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataArr count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellData = [_dataArr objectAtIndex:indexPath.row];
    
    MyTaskStorageViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell.titleLabel setText:[cellData objectForKey:@"name"]];
    [cell.abstractLabel setText:[cellData objectForKey:@"address"]];
    
    [cell.imgView sd_setImageWithURL:[cellData objectForKey:@"picUrl"] placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
    
    [cell.worktimeLabel setText:@""];
    cell.tag = [[cellData objectForKey:@"id"] integerValue];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellData = [_dataArr objectAtIndex:indexPath.row];

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CheckStorageDetailViewController *vc = [[CheckStorageDetailViewController alloc] init];
    vc.storageId = [cellData objectForKey:@"id"];
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
