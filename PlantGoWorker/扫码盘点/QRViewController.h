//
//  QRViewController.h
//  PlantGoWorker
//
//  Created by 纪翀 on 16/2/24.
//  Copyright © 2016年 青希羽博. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QRViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *cameraView;
@property (weak, nonatomic) IBOutlet UIView *boxView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quitBtn;
@property (weak, nonatomic) IBOutlet UILabel *hintLabel;
@property (weak, nonatomic) IBOutlet UITableView *tabView;

@property NSString *stockId;
@property NSString *storageId;

@end
