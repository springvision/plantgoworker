//
//  CheckStorageResourceViewController.m
//  PlantGoWorker
//
//  Created by 纪翀 on 2017/11/4.
//  Copyright © 2017年 m-training. All rights reserved.
//

#import "CheckStorageResourceViewController.h"
#import "MyTaskProductViewCell.h"

@interface CheckStorageResourceViewController ()
{
    NSArray *_dataArr;
}
@end

@implementation CheckStorageResourceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_imgView sd_setImageWithURL:[NSURL URLWithString:_picUrl] placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
    [_titleLabel setText:_name];
    [_nameLabel setText:_name];
    [self initTabView];
    [self loadData];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData{
    NSString *token = [GlobalVar getVarName:@"token"];
    NSDictionary *postDic = @{
                              @"token":token,
                              @"resConfId":_resConfId,
                              @"storageId":_storageId
                              };
    [SendRequestClass commonRequest:@"/api/storageproductdetail" postDic:postDic completionBlock:^(NSDictionary *succDic) {
        if ([[succDic objectForKey:@"code"] intValue] == SUCC_RESP)
        {
            NSLog(@"Success storageproducttasks");
            _dataArr = [[succDic objectForKey:@"data"] objectForKey:@"resIds"];
            [_countLabel setText:[NSString stringWithFormat:@"%d颗", [_dataArr count]]];
        
            [_tabView reloadData];
        }
        else{
            NSLog(@"Fail storageproducttasks");
            [MyHint showHint:[succDic objectForKey:@"message"]];
        }
        
    } failureBlock:^(NSDictionary *failDic) {
        NSLog(@"Network error");
        [MyHint showHint:@"网络出现故障"];
    }];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - UITabView
- (void)initTabView
{
    _tabView.rowHeight = 44;
    //    UIView * view = [[UIView alloc] initWithFrame:CGRectZero];
    //    [_tabView setTableHeaderView:view];
    _tabView.bounces = NO;
    [_tabView registerNib:[UINib nibWithNibName:@"MyTaskProductViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_dataArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *barCode = [_dataArr objectAtIndex:indexPath.section];
    MyTaskProductViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell.barcodeLabel setText:barCode];
    return cell;
}

- (UIView *)headerView
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectZero];
    return view;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
